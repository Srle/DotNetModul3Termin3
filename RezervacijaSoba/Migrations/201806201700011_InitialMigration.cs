namespace RezervacijaSoba.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Rezervacijas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        imeIPrezime = c.String(nullable: false, maxLength: 50),
                        DatumOd = c.DateTime(nullable: false),
                        DatumDo = c.DateTime(nullable: false),
                        Otkazana = c.Boolean(nullable: false),
                        SobaRezervacijeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sobas", t => t.SobaRezervacijeId, cascadeDelete: true)
                .Index(t => t.SobaRezervacijeId);
            
            CreateTable(
                "dbo.Sobas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BrojSobe = c.Int(nullable: false),
                        BrojKreveta = c.Int(nullable: false),
                        CenaNocenja = c.Double(nullable: false),
                        SmestajSobaId = c.Int(nullable: false),
                        Otkazana = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Smestajs", t => t.SmestajSobaId, cascadeDelete: true)
                .Index(t => t.SmestajSobaId);
            
            CreateTable(
                "dbo.Smestajs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Naziv = c.String(nullable: false, maxLength: 50),
                        Opis = c.String(nullable: false, maxLength: 200),
                        Adresa = c.String(nullable: false, maxLength: 100),
                        Ocena = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sobas", "SmestajSobaId", "dbo.Smestajs");
            DropForeignKey("dbo.Rezervacijas", "SobaRezervacijeId", "dbo.Sobas");
            DropIndex("dbo.Sobas", new[] { "SmestajSobaId" });
            DropIndex("dbo.Rezervacijas", new[] { "SobaRezervacijeId" });
            DropTable("dbo.Smestajs");
            DropTable("dbo.Sobas");
            DropTable("dbo.Rezervacijas");
        }
    }
}
