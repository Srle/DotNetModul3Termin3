namespace RezervacijaSoba.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeSoba : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Sobas", "Otkazana");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Sobas", "Otkazana", c => c.Boolean(nullable: false));
        }
    }
}
