﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RezervacijaSoba.Models
{
    public class Smestaj
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Naziv { get; set; }

        [Required]
        [StringLength(200)]
        public string Opis { get; set; }

        [Required]
        [StringLength(100)]
        public string Adresa { get; set; }

        [Range(1, 5)]
        public int Ocena { get; set; }

        public List<Soba> Sobe { get; set; }

    }
}