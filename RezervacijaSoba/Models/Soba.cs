﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RezervacijaSoba.Models
{
    public class Soba
    {
        public int Id { get; set; }

        public int BrojSobe { get; set; }

        public int BrojKreveta { get; set; }

        public double CenaNocenja { get; set; }

        public int SmestajSobaId { get; set; }

        [ForeignKey("SmestajSobaId")]
        public Smestaj SmestajSoba { get; set; }

        public List<Rezervacija> RezervacijeSoba { get; set; }

    }
}