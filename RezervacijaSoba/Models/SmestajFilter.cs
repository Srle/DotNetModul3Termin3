﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaSoba.Models
{
    public class SmestajFilter
    {
        public string SmestajNaziv { get; set; }
        public string SmestajAdresa { get; set; }

    }
}