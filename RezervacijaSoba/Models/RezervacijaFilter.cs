﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RezervacijaSoba.Models
{
    public class RezervacijaFilter
    {
        [DataType(DataType.Date)]
        public DateTime? RezervacijaDatumOd { get; set; }

        [DataType(DataType.Date)]
        public DateTime? RezervacijaDatumDo { get; set; }

        public int? BrojSobeRezervacije { get; set; }

    }
}