﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RezervacijaSoba.Models
{
    public class SmestajContext : DbContext
    {
        public DbSet<Smestaj> Smestajs { get; set; }
        public DbSet<Rezervacija> rezervacijas { get; set; }
        public DbSet<Soba> Sobas { get; set; }

        public SmestajContext() : base("name=RezervacijaSobaContext")
        {

        }

    }
}