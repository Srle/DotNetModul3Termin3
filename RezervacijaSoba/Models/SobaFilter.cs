﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaSoba.Models
{
    public class SobaFilter
    {
        public int? SobaBrojKreveta { get; set; }
        public double? SobaCenaNocenja { get; set; }
        public string SmestajSobe { get; set; }

    }
}