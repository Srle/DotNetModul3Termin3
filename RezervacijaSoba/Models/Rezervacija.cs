﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RezervacijaSoba.Models
{
    public class Rezervacija
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string imeIPrezime { get; set; }

        [DataType(DataType.Date)]
        public DateTime DatumOd { get; set; }

        [DataType(DataType.Date)]
        public DateTime DatumDo { get; set; }

        public bool Otkazana { get; set; }

        public int SobaRezervacijeId { get; set; }

        [ForeignKey("SobaRezervacijeId")]
        public Soba SobaRezervacije { get; set; }
    }
}