﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using RezervacijaSoba.Models;

namespace RezervacijaSoba.Controllers
{
    public class RezervacijasController : Controller
    {
        private SmestajContext db = new SmestajContext();

        public static List<string> SortTypes = new List<string>
        {
            "Broj Kreveta" ,
            "Broj Kreveta Descending",
            "Cena Nocenja",
            "Cena Nocenja Descending",
        };

        private int itemsPerPage = 3;

        // GET: Rezervacijas
        public ActionResult Index(string sortType = "Broj Kreveta", int page = 1)
        {
            IQueryable<Rezervacija> rezervacijas = db.rezervacijas.Include(r => r.SobaRezervacije);

            switch (sortType)
            {
                case "Broj Kreveta":
                    rezervacijas = rezervacijas.OrderBy(x => x.SobaRezervacije.BrojKreveta);
                    break;
                case "Broj Kreveta Descending":
                    rezervacijas = rezervacijas.OrderByDescending(x => x.SobaRezervacije.BrojKreveta);
                    break;
                case "Cena Nocenja":
                    rezervacijas = rezervacijas.OrderBy(x => x.SobaRezervacije.CenaNocenja);
                    break;
                case "Cena Nocenja Descending":
                    rezervacijas = rezervacijas.OrderByDescending(x => x.SobaRezervacije.CenaNocenja);
                    break;
            }

            ViewBag.sortTypes = new SelectList(SortTypes, sortType);
            ViewBag.CurrentSortType = sortType;

            return View(rezervacijas.ToPagedList(page, itemsPerPage));
        }

        // GET: Rezervacijas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rezervacija rezervacija = db.rezervacijas.Find(id);
            if (rezervacija == null)
            {
                return HttpNotFound();
            }
            return View(rezervacija);
        }

        // GET: Rezervacijas/Create
        public ActionResult Create(int? id)
        {
            if (id != null)
            {
                Soba s = db.Sobas.Find(id);
                List<Soba> soba = new List<Soba>();
                soba.Add(s);
                ViewBag.SobaRezervacijeId = new SelectList(soba, "Id", "BrojSobe");
            }
            else
            {
                ViewBag.SobaRezervacijeId = new SelectList(db.Sobas, "Id", "BrojSobe");
            }
            return View();
        }

        // POST: Rezervacijas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,imeIPrezime,DatumOd,DatumDo,Otkazana,SobaRezervacijeId")] Rezervacija rezervacija)
        {
            if (ModelState.IsValid)
            {
                db.rezervacijas.Add(rezervacija);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SobaRezervacijeId = new SelectList(db.Sobas, "Id", "Id", rezervacija.SobaRezervacijeId);
            return View(rezervacija);
        }

        // GET: Rezervacijas/Edit/5
        public ActionResult Edit(int? id, int? sid)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rezervacija rezervacija = db.rezervacijas.Find(id);
            if (rezervacija == null)
            {
                return HttpNotFound();
            }

            if (sid != null)
            {
                Soba s = db.Sobas.Find(id);
                List<Soba> soba = new List<Soba>();
                soba.Add(s);
                ViewBag.SobaRezervacijeId = new SelectList(soba, "Id", "BrojSobe");
            }
            else
            {
                ViewBag.SobaRezervacijeId = new SelectList(db.Sobas, "Id", "BrojSobe", rezervacija.SobaRezervacijeId);
            }
            return View(rezervacija);

        }

        // POST: Rezervacijas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,imeIPrezime,DatumOd,DatumDo,Otkazana,SobaRezervacijeId")] Rezervacija rezervacija)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rezervacija).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SobaRezervacijeId = new SelectList(db.Sobas, "Id", "Id", rezervacija.SobaRezervacijeId);
            return View(rezervacija);
        }

        // GET: Rezervacijas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rezervacija rezervacija = db.rezervacijas.Find(id);
            if (rezervacija == null)
            {
                return HttpNotFound();
            }
            return View(rezervacija);
        }

        // POST: Rezervacijas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Rezervacija rezervacija = db.rezervacijas.Find(id);
            db.rezervacijas.Remove(rezervacija);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //Filter:
        [HttpPost]
        public ActionResult Filter(RezervacijaFilter filter)
        {
            IQueryable<Rezervacija> rezervacijas = db.rezervacijas.Include(r => r.SobaRezervacije);

            if (filter.RezervacijaDatumOd != null && filter.RezervacijaDatumOd != DateTime.MinValue)
            {
                rezervacijas = rezervacijas.Where(s => s.DatumOd >= filter.RezervacijaDatumOd);
            }

            if (filter.RezervacijaDatumDo != null && filter.RezervacijaDatumDo != DateTime.MinValue)
            {
                rezervacijas = rezervacijas.Where(s => s.DatumDo <= filter.RezervacijaDatumDo);
            }

            if (filter.BrojSobeRezervacije != null)
            {
                rezervacijas = rezervacijas.Where(p => p.SobaRezervacije.BrojSobe == filter.BrojSobeRezervacije);
            }

            var rezervacijeKojeNisuOtkazane = rezervacijas.Where(x => x.Otkazana == false); // ako nisu soft deleted

            if (rezervacijeKojeNisuOtkazane.Any()) // ukoliko ih ima da nisu otkazane u listi vraca view sa listom
            {
                return View(rezervacijeKojeNisuOtkazane.ToList());
            }
            TempData["NemaZadatogKriterijuma"] = "Izaberite drugacije vrednosti u filteru, padaci po ovom kriterijumu ne postoje u bazi!";
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
