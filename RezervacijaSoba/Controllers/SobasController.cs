﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using PagedList;
using RezervacijaSoba.Models;

namespace RezervacijaSoba.Controllers
{
    public class SobasController : Controller
    {
        private SmestajContext db = new SmestajContext();

        public static List<string> SortTypes = new List<string>
        {
            "Broj Kreveta" ,
            "Broj Kreveta Descending",
            "Cena Nocenja",
            "Cena Nocenja Descending",
        };

        private int itemsPerPage = 3;

        // GET: Sobas
        public ActionResult Index(string sortType = "Broj Kreveta", int page = 1)
        {   
            IQueryable<Soba> sobas = db.Sobas.Include(s => s.SmestajSoba);

            switch (sortType)
            {
                case "Broj Kreveta":
                    sobas = sobas.OrderBy(x => x.BrojKreveta);
                    break;
                case"Broj Kreveta Descending":
                    sobas = sobas.OrderByDescending(x => x.BrojKreveta);
                    break;
                case "Cena Nocenja":
                    sobas = sobas.OrderBy(x => x.CenaNocenja);
                    break;
                case "Cena Nocenja Descending":
                    sobas = sobas.OrderByDescending(x => x.CenaNocenja);
                    break;
            }

            ViewBag.sortTypes = new SelectList(SortTypes, sortType);
            ViewBag.CurrentSortType = sortType;

            return View(sobas.ToPagedList(page, itemsPerPage));
        }

        // GET: Sobas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Soba soba = db.Sobas.Find(id);
            if (soba == null)
            {
                return HttpNotFound();
            }
            var rezervacije = db.rezervacijas.ToList(); //Ovo dodaje sobe za prikaz u detalje od smeštaja

            return View(soba);
        }

        // GET: Sobas/Create
        public ActionResult Create(int? id)
        {
                    if (id != null)
            {
                Smestaj s = db.Smestajs.Find(id);
                List<Smestaj> smestaj = new List<Smestaj>();
                smestaj.Add(s);
                ViewBag.SmestajSobaId = new SelectList(smestaj, "Id", "Naziv");
            }
            else
            {
                ViewBag.SmestajSobaId = new SelectList(db.Smestajs, "Id", "Naziv");
            }
            return View();
        }

        // POST: Sobas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,BrojSobe,BrojKreveta,CenaNocenja,SmestajSobaId,Otkazana")] Soba soba)
        {
            if (ModelState.IsValid)
            {
                db.Sobas.Add(soba);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SmestajSobaId = new SelectList(db.Smestajs, "Id", "Naziv", soba.SmestajSobaId);
            return View(soba);
        }

        // GET: Sobas/Edit/5
        public ActionResult Edit(int? id, int? sid)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Soba soba = db.Sobas.Find(id);
            if (soba == null)
            {
                return HttpNotFound();
            }

            if (sid != null)
            {
                Smestaj s = db.Smestajs.Find(id);
                List<Smestaj> smestaj = new List<Smestaj>();
                smestaj.Add(s);
                ViewBag.SmestajSobaId = new SelectList(smestaj, "Id", "Naziv");
            }
            else
            {
                ViewBag.SmestajSobaId = new SelectList(db.Smestajs, "Id", "Naziv", soba.SmestajSobaId);
            }
            return View(soba);
        }

        // POST: Sobas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,BrojSobe,BrojKreveta,CenaNocenja,SmestajSobaId,Otkazana")] Soba soba)
        {
            if (ModelState.IsValid)
            {
                db.Entry(soba).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SmestajSobaId = new SelectList(db.Smestajs, "Id", "Naziv", soba.SmestajSobaId);
            return View(soba);
        }

        // GET: Sobas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Soba soba = db.Sobas.Find(id);
            if (soba == null)
            {
                return HttpNotFound();
            }
            return View(soba);
        }

        // POST: Sobas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Soba soba = db.Sobas.Find(id);
            db.Sobas.Remove(soba);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //Filter:
        [HttpPost]
        public ActionResult Filter(SobaFilter filter)
        {
            IQueryable<Soba> sobas = db.Sobas.Include(s => s.SmestajSoba);

            if (filter.SobaBrojKreveta != null)
            {
                sobas = sobas.Where(s => s.BrojKreveta == filter.SobaBrojKreveta);
            }

            if (filter.SobaCenaNocenja != null)
            {
                sobas = sobas.Where(s => s.CenaNocenja == filter.SobaCenaNocenja);
            }

            if (!filter.SmestajSobe.IsNullOrWhiteSpace())
            {
                sobas = sobas.Where(p => p.SmestajSoba.Naziv.Contains(filter.SmestajSobe));
            }

            return View(sobas.ToList());
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
